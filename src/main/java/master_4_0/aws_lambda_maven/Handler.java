package master_4_0.aws_lambda_maven;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Base64;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.google.gson.Gson;

public class Handler implements RequestStreamHandler {
	
	AmazonS3 s3client = AmazonS3ClientBuilder
			  .standard()
			  .withRegion(Regions.EU_NORTH_1)
			  .build();
	
	String bucketName = "master-4.0-stefan-nikolic-15-2019";
	
	Gson gson = new Gson();
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public void handleRequest(InputStream input, OutputStream output, Context context) throws IOException {
		
		if(!s3client.doesBucketExist(bucketName)) {
		    s3client.createBucket(bucketName);
		}
		
		JSONParser parser = new JSONParser();
	    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
	    JSONObject responseJson = new JSONObject();
	    
	    HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "text/plain");        
	    
	    try {
	        JSONObject event = (JSONObject) parser.parse(reader);
	 
	        switch (event.get("httpMethod").toString()) {
			case "POST":
				String name = gson.fromJson(event.get("body").toString(), HashMap.class).get("name").toString();
				String file = gson.fromJson(event.get("body").toString(), HashMap.class).get("file").toString();
				File resultFile = new File("/tmp/" + name);
				byte[] data = Base64.getDecoder().decode(file);
				try (OutputStream stream = new FileOutputStream(resultFile)) {
				    stream.write(data);
				    stream.close();
				    s3client.putObject(bucketName, name, resultFile);
				    responseJson.put("statusCode", 200);
				    responseJson.put("body", "ok");
				}
				catch (Exception e) {
					responseJson.put("statusCode", 500);
					responseJson.put("body", e.toString());
				}
				break;
			case "PUT":
				String oldName = gson.fromJson(event.get("body").toString(), HashMap.class).get("oldName").toString();
				String newname = gson.fromJson(event.get("body").toString(), HashMap.class).get("newName").toString();
				try {
					s3client.copyObject(bucketName, oldName, bucketName, newname);
					s3client.deleteObject(bucketName, oldName);
					responseJson.put("statusCode", 200);
				    responseJson.put("body", "ok");
				}
				catch (Exception e) {
					responseJson.put("statusCode", 500);
					responseJson.put("body", e.toString());
				}
				break;
			case "GET":
				String fileName = gson.fromJson(event.get("queryStringParameters").toString(), HashMap.class).get("name").toString();
				try {
					S3Object s3object = s3client.getObject(bucketName, fileName);
					S3ObjectInputStream inputStream = s3object.getObjectContent();
					FileOutputStream fos = new FileOutputStream(new File("/tmp/" + fileName));
					byte[] read_buf = new byte[1024];
					int read_len = 0;
				    while ((read_len = inputStream.read(read_buf)) > 0) {
				        fos.write(read_buf, 0, read_len);
				    }
				    inputStream.close();
				    fos.close();
				    header.put("Content-Disposition", "attachment; filename=" + fileName);
				    responseJson.put("statusCode", 200);
				    responseJson.put("body", new String(Files.readAllBytes(new File("/tmp/" + fileName).toPath()), StandardCharsets.UTF_8));
				}
				catch (Exception e) {
					responseJson.put("statusCode", 500);
					responseJson.put("body", e.toString());
				}
				break;
				
			case "DELETE":
				String dltName = gson.fromJson(event.get("queryStringParameters").toString(), HashMap.class).get("name").toString();
				try {
					s3client.deleteObject(bucketName, dltName);
					responseJson.put("statusCode", 200);
				    responseJson.put("body", "ok");
				} catch (Exception e) {
					responseJson.put("statusCode", 500);
					responseJson.put("body", e.toString());
				}
				break;
				
			default:
				break;
			}
	        
	        responseJson.put("headers", header);
	        if(header.get("Content-Disposition") == null)
	        	responseJson.put("isBase64Encoded", false);
	        else
	        	responseJson.put("isBase64Encoded", true);
	        
	        output.write(responseJson.toString().getBytes());
	    }
	    catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
}
