package master_4_0.aws_lambda_maven;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;

public class Counter implements RequestHandler<S3Event, String> {
	
	public static Integer id = 0;
	
	AmazonS3 s3client = AmazonS3ClientBuilder
			  .standard()
			  .withRegion(Regions.EU_NORTH_1)
			  .build();
	AmazonDynamoDB client = AmazonDynamoDBClientBuilder.defaultClient();
	DynamoDB dynamoDB = new DynamoDB(client);
	Table table = dynamoDB.getTable("stefan-s3-dynamodb-handler");
	
	@Override
	public String handleRequest(S3Event input, Context context) {
		S3EventNotification.S3EventNotificationRecord record = input.getRecords().get(0);
		String bucket = record.getS3().getBucket().getName();
		String file = record.getS3().getObject().getKey();
		
		String response = "ok";
		
		switch (record.getEventName()) {
			case "ObjectCreated:Put":
			case "ObjectCreated:Copy":
				
				FileOutputStream fos;
		
				try {
					S3Object s3object = s3client.getObject(bucket, file);
					S3ObjectInputStream inputStream = s3object.getObjectContent();
					fos = new FileOutputStream(new File("/tmp/" + file));
					byte[] read_buf = new byte[1024];
					int read_len = 0;
				    while ((read_len = inputStream.read(read_buf)) > 0) {
				        fos.write(read_buf, 0, read_len);
				    }
				    inputStream.close();
				    fos.close();
				} catch (Exception e) {
					response = e.toString();
				}
				
				Integer lines = 0, words = 0, chars = 0;
				
				try {
					lines = countLines(new File("/tmp/" + file));
					words = countWords(new File("/tmp/" + file));
					chars = countChars(new File("/tmp/" + file));
					
					table.putItem(new PutItemSpec().withItem(new Item()
							.with("id", id++)
							.with("ime_fajla", file)
							.with("br_redova", lines)
							.with("br_reci", words)
							.with("br_karaktera", chars)));
				} catch (IOException e) {
					response = e.toString();
				}
				
				break;
			
			case "ObjectRemoved:Delete":
				
				try {
					HashMap<String, String> nameMap = new HashMap<String, String>();
			        nameMap.put("#ime", "ime_fajla");
			        HashMap<String, Object> valueMap = new HashMap<String, Object>();
			        valueMap.put(":ime", file);
			        
			        QuerySpec querySpec = new QuerySpec().withKeyConditionExpression("#ime = :ime").withNameMap(nameMap)
			                .withValueMap(valueMap);
			        
			        ItemCollection<QueryOutcome> items = null;
			        Iterator<Item> iterator = null;
			        Item item = null;
			        
			        items = table.query(querySpec);
			        
			        Integer id = 0;
			        
			        if(items.getAccumulatedItemCount() > 1)
			        	response = "More than one item!";
			        else {
			        	iterator = items.iterator();
			        	while (iterator.hasNext()) {
			                item = iterator.next();
			                id = item.getInt("id");
			            }
			        	table.deleteItem(new DeleteItemSpec().withPrimaryKey(
			        			new PrimaryKey("id", id)));
			        }   	
				}
				catch (Exception e) {
					response = e.toString();
				}
				
				break;

			default:
				break;
		}
		System.out.println(response);
		return response;
	}

	public Integer countLines(File file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		int lines = 0;
		while (reader.readLine() != null) lines++;
		reader.close();
		return lines;
	}
	
	public Integer countWords(File file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		int words = 0;
		String line;
		while ((line = reader.readLine()) != null) {
			String[] wordList = line.split("\\s+");
			words += wordList.length;
		}
		reader.close();
		return words;
	}
	
	public Integer countChars(File file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		int chars = 0;
		String line;
		while ((line = reader.readLine()) != null) {
			chars += line.length();
		}
		reader.close();
		return chars;
	}

}
